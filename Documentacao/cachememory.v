//Modulo que converte o numero n decimal para o numero n_hex hexadecimal.
module convert_to_hexadecimal(
	input [6:0]n, 
	output reg [6:0]n_hex
);
	always @(n) begin
		 case(n)
			0: n_hex <= 7'b0000001;	//0
			1: n_hex <= 7'b1001111;	//1
			2: n_hex <= 7'b0010010;	//2
			3: n_hex <= 7'b0000110;	//3
			4: n_hex <= 7'b1001100;	//4
			5: n_hex <= 7'b0100100;	//5
			6: n_hex <= 7'b0100000;	//6
			7: n_hex <= 7'b0001111;	//7
			8: n_hex <= 7'b0000000;	//8
			9: n_hex <= 7'b0001100;	//9
			10:n_hex <= 7'b0001000;	//A	10
			11:n_hex <= 7'b1100000;	//B	11
			12:n_hex <= 7'b0110001;	//C	12
			13:n_hex <= 7'b1000010;	//D	13
			14:n_hex <= 7'b0110000;	//E	14
			15:n_hex <= 7'b0111000;	//F	15
			default n_hex <=7'b1111111;
		 endcase
	end
endmodule

//Modulo que implementa a divisao dos bits do numero n, para exibido-lo utilizando dois displays de 7 segmentos.
module display(
	input [7:0] n,
	output reg [6:0] ML_HEXO,
	output reg [6:0] ML_HEX1
);
	always @(n)begin
		if(n > 15) begin
			ML_HEXO <= n%16; ML_HEX1 <= n/16;
		end
		else begin
				ML_HEXO <= n; ML_HEX1 <= 0;
		end
	end
endmodule

//Modulo que implementa a memoria cache associativa por duas vias.
module cache_data(
	input clk,
	input [7:0]req_addr, input [7:0]req_data, input req_we,
	output reg [7:0]data_read,
	output reg data_hit, output reg data_miss
);
	//Index de entrada, que e definido pelo 3 bits mais significativos do endereco de parametro.
	wire [2:0]data_index = req_addr[7:5];
	//Tag de entrada, que e definido pelos bits entre 4 e 1 mais significativos do endereco de parametro.
	wire [3:0]data_tag = req_addr[4:1];
	//Memoria cache implementada a partir de array bidimensional de 14 bits.
	reg [13:0] CACHE [0:7][0:1];

	integer i, j;
	reg LRU;    //Variavel para definir a operacao de LRU.
	
	//E carregado valores na memoria cache.
	initial begin
		for(i = 0; i < 8; i = i+1) begin
			for(j = 0; j < 2; j = j+1) begin
				CACHE[i][j][7:0] = (i+1)*(j+1);	//Define os dados com os valor i*j.
				CACHE[i][j][8] = 1'b1;		//Define os bits de validade com 1.
				CACHE[i][j][13:10] = j;	//Define a tag com j.
			end
			CACHE[i][0][9] = 1'b0;	//Define o LRU da via_0 com 0.
			CACHE[i][1][9] = 1'b1;	//Define o LRU da via_1 com 1.
		 end
	end
	
	always @(posedge clk) begin
		if(req_we) begin 	//Caso a acao seja de escrita.
			if(CACHE[data_index][0][13:10] != data_tag && CACHE[data_index][1][13:10] != data_tag) begin	//Caso a tag de parametro da via_0 e da via_1 sejam diferentes da tag de parametro, ocorrera um miss, e portanto a escrita na memoria cache.
				if(CACHE[data_index][0][9] == 0) begin	//Caso o bit LRU da via_0 seja igual a 0.
					LRU = 1'b1; //Define a entrada para a via_1;
				end
				else begin	//Caso o bit LRU da via_0 seja igual a 1.
					LRU = 1'b0;	//Define que devera ser escrito na via_0;
				end
				CACHE[data_index][LRU][7:0] = req_data;	//Escreve o dado no index, tag e offset de parametro.
				CACHE[data_index][LRU][8] = 1'b1;	//Define 1 para o bit de validade do dado selecionado.
				CACHE[data_index][LRU][9] = 1'b0;	//Define 0 para o bit escrito mais recetemente, ou seja, o dado selecionado.
				CACHE[data_index][~LRU][9] = 1'b1;	//Define 1 para o bit escrito menos recetemente, ou seja, o dado nao selecionado.
				CACHE[data_index][LRU][13:10] = data_tag;	//Define a nova tag para a linha de cache selecionada.
				data_hit = 1'b0;	//Define que nao ocorreu um hit.
				data_miss = 1'b1;	//Define que ocorreu um miss.
			end
			else begin	//Caso a tag de parametro seja igual da tag encontrada no endereco selecionado, ocorrera um hit.
				data_hit = 1'b1;	//Define que ocorreu um hit.
				data_miss = 1'b0;	//Define que nao ocorreu um miss.
			end
		end
		else begin	//Caso a acao seja de leitura.
			if(CACHE[data_index][0][13:10] == data_tag) begin //Caso a tag da via_0 seja igual a tag encontrada no endereco, ocorrera um hit.
				if(CACHE[data_index][0][8] == 1)	begin //Caso o bit de validade no endereco seja 1.
					data_read = CACHE[data_index][0][7:0];	//Ocorre a leitura do dado no index e no bloco selecionados.
					data_hit = 1'b1;	//Define que ocorreu um hit.
					data_miss = 1'b0;	//Define que nao ocorreu um miss.
				end
			end
			else if(CACHE[data_index][1][13:10] == data_tag) begin //Caso a tag da via_1 seja igual da tag encontrada no endereco, ocorrera um hit.
				if(CACHE[data_index][1][8] == 1)	begin //Caso o bit de validade no endereco seja 1.
					data_read = CACHE[data_index][1][7:0];	//Ocorre a leitura do dado no index e no bloco selecionos.
					data_hit = 1'b1;	//Define que ocorreu um hit.
					data_miss = 1'b0;	//Define que nao ocorreu um miss.
				end
			end
			else begin	//Caso a tag diferente da tag encontrada no endereco, ocorrera um miss, portanto faz-se necessario a busca na memoria RAM, que nao foi implementada neste projeto.
				data_hit = 1'b0;	//Define nao ocorreu um hit.
				data_miss = 1'b1;	//Define que ocorreu um miss.
			end
		end
	end
endmodule

//Modulo principal.
module cache_memory(
	input [17:0]SW,
	output [0:6]HEX0, output [0:6]HEX1, output [0:6]HEX2, output [0:6]HEX3, output [0:6]HEX4, output [0:6]HEX5, output [0:6]HEX6, output [0:6]HEX7,
	output [17:0]LEDR
);
	wire [7:0] input_data, input_addr, output_cache;	
	wire [6:0] output_0, output_1;
	
	cache_data CD(SW[16], SW[15:8], SW[7:0], SW[17], output_cache, LEDR[0], LEDR[1]);
			
	//Utilizados para desligar os displays HEX2 e HEX3.
	assign HEX2 = 7'b1111111;
	assign HEX3 = 7'b1111111;
	//LEDR[17] sinaliza a acao de escrita.
	assign LEDR[17] = SW[17];
	//input_addr recebe o valor do endereco de entrada de cache_data.
	assign input_addr = SW[15:8];
	//input_data recebe o valor de dados de entrada de cache_data.
	assign input_data = SW[7:0];
	
	//Modulos para exibir os dados de entrada da memoria.
	display D0(input_data, output_0, output_1);
	convert_to_hexadecimal H0(output_0, HEX4);
	convert_to_hexadecimal H1(output_1, HEX5);
	//Modulos para exibir o endereco de entrada da memoria.
	display D1(input_addr, output_0, output_1);
	convert_to_hexadecimal H2(output_0, HEX6);
	convert_to_hexadecimal H3(output_1, HEX7);
	//Modulos para exibir os dados de saida da memoria.
	display D2(output_cache, output_0, output_1);
	convert_to_hexadecimal H4(output_0, HEX0);
	convert_to_hexadecimal H5(output_1, HEX1);
endmodule


