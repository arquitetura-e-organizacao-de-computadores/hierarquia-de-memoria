\select@language {brazil}
\select@language {english}
\select@language {brazil}
\contentsline {section}{\numberline {1}Metodologia}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Organiza\IeC {\c c}\IeC {\~a}o e Planejamento do projeto}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Ferramentas utilizadas}{2}{subsection.1.2}
\contentsline {section}{\numberline {2}Refer\IeC {\^e}ncias}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}M\IeC {\'o}dulos para convers\IeC {\~a}o dos dados de entrada e de sa\IeC {\'\i }da}{2}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}convert\_to\_hexadecimal}{2}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}display}{3}{subsubsection.2.1.2}
\contentsline {subsection}{\numberline {2.2}M\IeC {\'o}dulo de implementa\IeC {\c c}\IeC {\~a}o da mem\IeC {\'o}ria RAM com 32 palavras de 4 bits - parteI}{3}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}M\IeC {\'o}dulos de implementa\IeC {\c c}\IeC {\~a}o da mem\IeC {\'o}ria com a utiliza\IeC {\c c}\IeC {\~a}o do arquivo MIF - parte II}{5}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}M\IeC {\'o}dulos de implementa\IeC {\c c}\IeC {\~a}o da mem\IeC {\'o}ria cache associativa por conjunto de duas vias}{6}{subsection.2.4}
\contentsline {subsubsection}{\numberline {2.4.1}cache\_data}{6}{subsubsection.2.4.1}
\contentsline {subsubsection}{\numberline {2.4.2}cache\_memory}{9}{subsubsection.2.4.2}
\contentsline {section}{\numberline {3}Resultados obtidos}{10}{section.3}
\contentsline {subsection}{\numberline {3.1}parteI}{10}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}parteII}{11}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}parteIII}{12}{subsection.3.3}
\contentsline {section}{\numberline {4}Conclus\IeC {\~a}o}{13}{section.4}
