//Modulo que converte o numero n decimal para o numero n_hex hexadecimal.
module convert_to_hexadecimal(
	input [6:0]n, 
	output reg [6:0]n_hex
);
	always @(n) begin
		 case(n)
			0: n_hex <= 7'b0000001;	//0
			1: n_hex <= 7'b1001111;	//1
			2: n_hex <= 7'b0010010;	//2
			3: n_hex <= 7'b0000110;	//3
			4: n_hex <= 7'b1001100;	//4
			5: n_hex <= 7'b0100100;	//5
			6: n_hex <= 7'b0100000;	//6
			7: n_hex <= 7'b0001111;	//7
			8: n_hex <= 7'b0000000;	//8
			9: n_hex <= 7'b0001100;	//9
			10:n_hex <= 7'b0001000;	//A	10
			11:n_hex <= 7'b1100000;	//B	11
			12:n_hex <= 7'b0110001;	//C	12
			13:n_hex <= 7'b1000010;	//D	13
			14:n_hex <= 7'b0110000;	//E	14
			15:n_hex <= 7'b0111000;	//F	15
			default n_hex <=7'b1111111;
		 endcase
	end
endmodule

//Modulo que implementa a divisao dos bits do numero n, para exibido-lo utilizando dois displays de 7 segmentos.
module display(
	input [7:0] n,
	output reg [6:0] ML_HEXO,
	output reg [6:0] ML_HEX1
);
	always @(n)begin
		if(n > 15) begin
			ML_HEXO <= n%16; ML_HEX1 <= n/16;
		end
		else begin
				ML_HEXO <= n; ML_HEX1 <= 0;
		end
	end
endmodule
//Modulo que instancia a memoria ramlpm e implementa os inputs e outputs necessarios.
module parteI(
	input [17:0] SW,
	output [0:6]HEX0, output [0:6]HEX1, output [0:6]HEX2, output [0:6]HEX3, output [0:6]HEX4, output [0:6]HEX5, output [0:6]HEX6, output [0:6]HEX7,
	output [17:0]LEDR
);
	wire [7:0] out_ramlpm, addr;
	wire [6:0] output_0, output_1;

	ramlpm mem(SW[15:11], SW[16], SW[7:0], SW[17], out_ramlpm);

	//Utilizados para desligar os displays HEX2 e HEX3.
	assign HEX2 = 7'b1111111;
	assign HEX3 = 7'b1111111;
	//LEDR[17] sinaliza a acao de escrita.
	assign LEDR[17] = SW[17];
	//Addr recebe o valor do endereco de entrada de "ramlpm".
	assign addr = SW[15:11];

	//Modulos para exibir os dados de entrada da memoria.
	display D0(SW[7:0], output_0, output_1);
	convert_to_hexadecimal H0(output_0, HEX4);
	convert_to_hexadecimal H1(output_1, HEX5);
	//Modulos para exibir o endereco de entrada da memoria.
	display D1(addr, output_0, output_1);
	convert_to_hexadecimal H2(output_0, HEX6);
	convert_to_hexadecimal H3(output_1, HEX7);
	//Modulos para exibir os dados de saida da memoria.
	display D2(out_ramlpm, output_0, output_1);
	convert_to_hexadecimal H4(output_0, HEX0);
	convert_to_hexadecimal H5(output_1, HEX1);
endmodule
