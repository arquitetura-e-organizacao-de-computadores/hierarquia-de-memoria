library verilog;
use verilog.vl_types.all;
entity hexadecimal is
    port(
        ml_sig_dig      : in     vl_logic_vector(6 downto 0);
        D_HEXO          : out    vl_logic_vector(6 downto 0)
    );
end hexadecimal;
