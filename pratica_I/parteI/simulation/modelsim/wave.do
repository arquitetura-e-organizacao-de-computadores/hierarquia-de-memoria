onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /parteI/KEY0
add wave -noupdate /parteI/SW
add wave -noupdate /parteI/HEX7
add wave -noupdate /parteI/HEX6
add wave -noupdate /parteI/HEX5
add wave -noupdate /parteI/HEX4
add wave -noupdate /parteI/HEX3
add wave -noupdate /parteI/HEX2
add wave -noupdate /parteI/HEX1
add wave -noupdate /parteI/HEX0
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1310 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {1 ns}
view wave 
wave clipboard store
wave create -driver freeze -pattern random -initialvalue 000000000000000000 -period 50ps -random_type Uniform -seed 5 -range 17 0 -starttime 0ps -endtime 1000ps sim:/parteI/SW 
WaveExpandAll -1
wave create -driver expectedOutput -pattern clock -initialvalue 0 -period 50ps -dutycycle 50 -starttime 0ps -endtime 1000ps sim:/parteI/KEY0 
WaveCollapseAll -1
wave clipboard restore
