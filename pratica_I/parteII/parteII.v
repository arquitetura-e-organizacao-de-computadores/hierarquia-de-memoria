module hexadecimal(ml_sig_dig, D_HEXO );
	input [6:0] ml_sig_dig;
	output reg [6:0] D_HEXO;
	
	always @(ml_sig_dig) begin
		 case(ml_sig_dig)
			0:		D_HEXO <= 7'b0000001;	//0
			1:		D_HEXO <= 7'b1001111;	//1
			2:		D_HEXO <= 7'b0010010;	//2
			3:		D_HEXO <= 7'b0000110;	//3
			4:		D_HEXO <= 7'b1001100;	//4
			5:		D_HEXO <= 7'b0100100;	//5
			6:		D_HEXO <= 7'b0100000;	//6
			7:		D_HEXO <= 7'b0001111;	//7
			8:		D_HEXO <= 7'b0000000;	//8
			9:		D_HEXO <= 7'b0001100;	//9
			10:	D_HEXO <= 7'b0001000;	//A	10
			11:	D_HEXO <= 7'b1100000;	//B	11
			12:	D_HEXO <= 7'b0110001;	//C	12
			13:	D_HEXO <= 7'b1000010;	//D	13
			14:	D_HEXO <= 7'b0110000;	//E	14
			15:	D_HEXO <= 7'b0111000;	//F	15
			default D_HEXO <=7'b1111111;
		 endcase
	end
endmodule

module display_7seg(
	input [7:0] data,
	output reg [6:0] ML_HEXO,
	output reg [6:0] ML_HEX1 );

	always@(data)begin
		if(data > 15) begin
			ML_HEXO <= data%16;		//Número menos significativo
			ML_HEX1 <= data/16;		//Número mais significativo
		end
		else begin
				ML_HEXO <= data;
				ML_HEX1 <= 0;
		end
	end
endmodule
module parteII(
	input [17:0] SW,
	output [0:6] HEX0, output [0:6] HEX1, output [0:6] HEX2, output [0:6] HEX3, output [0:6] HEX4, output [0:6] HEX5, output [0:6] HEX6, output [0:6] HEX7,
	output [1:0]KEY,
	output [8:0]LEDG
);
	wire [7:0] out_ramlpm;	
	wire [6:0] out_data_1, out_data_2, out_addr_1, out_addr_2, out_ramlpm_data_1, out_ramlpm_data_2;
	wire [7:0] addr;
	
	ramlpm mem(SW[15:11], SW[16], SW[7:0], SW[17], out_ramlpm);
		assign HEX2 = 7'b1111111;
		assign HEX3 = 7'b1111111;
		assign LEDG[0] = SW[17]; //Write Signal
		assign addr = SW[15:11];
		assign LEDG[1] = SW[16];
		//Entrada de dados da mémoria
		display_7seg D0(SW[7:0], out_data_1, out_data_2);
		hexadecimal H0(out_data_1, HEX4);
		hexadecimal H1(out_data_2, HEX5);
		//Endereço
		display_7seg D1(addr, out_addr_1, out_addr_2); //converter
		hexadecimal H2(out_addr_1, HEX6);
		hexadecimal H3(out_addr_2, HEX7);
		//Saída de dados da mémoria
		display_7seg D2(out_ramlpm, out_ramlpm_data_1, out_ramlpm_data_2);
		hexadecimal H4(out_ramlpm_data_1, HEX0);
		hexadecimal H5(out_ramlpm_data_2, HEX1);
		
endmodule
