library verilog;
use verilog.vl_types.all;
entity display_7seg is
    port(
        data            : in     vl_logic_vector(7 downto 0);
        ML_HEXO         : out    vl_logic_vector(6 downto 0);
        ML_HEX1         : out    vl_logic_vector(6 downto 0)
    );
end display_7seg;
