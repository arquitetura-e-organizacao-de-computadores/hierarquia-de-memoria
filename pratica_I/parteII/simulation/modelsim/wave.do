onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /parteII/KEY0
add wave -noupdate /parteII/SW
add wave -noupdate /parteII/HEX7
add wave -noupdate /parteII/HEX6
add wave -noupdate /parteII/HEX5
add wave -noupdate /parteII/HEX4
add wave -noupdate /parteII/HEX1
add wave -noupdate /parteII/HEX0
add wave -noupdate /parteII/LEDG
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {250 ps}
view wave 
wave clipboard store
wave create -driver expectedOutput -pattern clock -initialvalue 0 -period 100ps -dutycycle 50 -starttime 0ps -endtime 1000ps sim:/parteII/KEY0 
wave create -driver freeze -pattern repeater -initialvalue 000000000000000000 -period 50ps -sequence { 000000000000000000 000000100000000000 000000000000000000 000001100000000000 000000000000000000 000000000000000000  } -repeat 20 -range 17 0 -starttime 0ps -endtime 1000ps sim:/parteII/SW 
WaveExpandAll -1
wave modify -driver freeze -pattern repeater -initialvalue 000000000000000000 -period 50ps -sequence { 000000000000000000 000000000000000001 010000000000000001 000000000000000000 000000000000000000 000000000000000000  } -repeat 20 -range 17 0 -starttime 0ps -endtime 1000ps Edit:/parteII/SW 
wave modify -driver freeze -pattern repeater -initialvalue 000000000000000000 -period 50ps -sequence { 000000000000000000 000000000000000001 010000000000000001 010000000000000001 000000000000000000 000000000000000000  } -repeat 20 -range 17 0 -starttime 0ps -endtime 1000ps Edit:/parteII/SW 
wave modify -driver freeze -pattern repeater -initialvalue 000000000000000000 -period 49ps -sequence { 000000000000000000 000000000000000001 010000000000000001 010000000000000001 000000000000000000 000000000000000000  } -repeat 20 -range 17 0 -starttime 0ps -endtime 1000ps Edit:/parteII/SW 
wave modify -driver freeze -pattern repeater -initialvalue 000000000000000000 -period 49ps -sequence { 000000000000000000 000000000000000001 010000000000000001 110000000000000001 000000000000000011 000000000000000000  } -repeat 20 -range 17 0 -starttime 0ps -endtime 1000ps Edit:/parteII/SW 
wave modify -driver freeze -pattern repeater -initialvalue 000000000000000000 -period 49ps -sequence { 000000000000000000 000000000000000001 100000000000000001 110000000000000001 000000000000000011 000000000000000000  } -repeat 20 -range 17 0 -starttime 0ps -endtime 1000ps Edit:/parteII/SW 
WaveCollapseAll -1
wave clipboard restore
