//Modulo que converte o número n decimal para o número n_hex hexadecimal.
module convert_to_hexadecimal(
	input [6:0]n, 
	output reg [6:0]n_hex
);
	always @(n) begin
		 case(n)
			0:		n_hex <= 7'b0000001;	//0
			1:		n_hex <= 7'b1001111;	//1
			2:		n_hex <= 7'b0010010;	//2
			3:		n_hex <= 7'b0000110;	//3
			4:		n_hex <= 7'b1001100;	//4
			5:		n_hex <= 7'b0100100;	//5
			6:		n_hex <= 7'b0100000;	//6
			7:		n_hex <= 7'b0001111;	//7
			8:		n_hex <= 7'b0000000;	//8
			9:		n_hex <= 7'b0001100;	//9
			10:	n_hex <= 7'b0001000;	//A	10
			11:	n_hex <= 7'b1100000;	//B	11
			12:	n_hex <= 7'b0110001;	//C	12
			13:	n_hex <= 7'b1000010;	//D	13
			14:	n_hex <= 7'b0110000;	//E	14
			15:	n_hex <= 7'b0111000;	//F	15
			default n_hex <=7'b1111111;
		 endcase
	end
endmodule

//Modulo que implementa a divisão dos bits do número n, para exibido-lo utilizando dois displays de 7 segmentos.
module display(
	input [7:0] n,
	output reg [6:0] ML_HEXO,
	output reg [6:0] ML_HEX1
);
	always @(n)begin
		if(n > 15) begin
			ML_HEXO <= n%16;		//Número menos significativo
			ML_HEX1 <= n/16;		//Número mais significativo
		end
		else begin
				ML_HEXO <= n;
				ML_HEX1 <= 0;
		end
	end
endmodule

//Modulo que implementa a memória cache associativa por duas vias.
module cache_data(
	input clk,							//Clock.
	input [7:0] req_addr,			//Endereço.
	input [7:0] req_data,			//Dado de entrada na memória cache.
	input req_we,						//Sinal que define a escrita na memória cache.
	output reg[7:0] data_read,		//Dado de sáida da memória cache.
	output reg data_hit,				//Bit para expressar a ação de hit na memória cache.
	output reg data_miss				//Bit para expressar a ação de miss na memória cache.
);
	wire [2:0]data_index = req_addr[7:5];	//Index de entrada, que é definido pelo 3 bits mais significativos do endereço de parâmetro.
	wire [3:0]data_tag = req_addr[4:1];		//Tag de entrada, que é definido pelos bits entre 4 e 1 mais significativos do endereço de parâmetro.
	
	//A cache é definida da seguinte forma: [nº de bits de dados] CACHE [nº bits de index][nº de bits do bloco].
	
	reg [13:0] CACHE [0:7][0:1];
	
	integer i, j;
	reg LRU;
	
	//É carregado valores contidos no arquivo .MIF na memória cache.
	initial begin
		for(i = 0; i < 8; i = i+1) begin
			for(j = 0; j < 2; j = j+1) begin
				CACHE[i][j][7:0] = (i+1)*(j+1);	//Define os dados com os valor i*j.
				CACHE[i][j][8] = 1'b1;		//Define os bits de validade com 1.
				CACHE[i][j][13:10] = j;	//Define 0 para a tag.
			end
			CACHE[i][0][9] = 1'b0;				//Define os bits de LRU da via_1 com 1, implementando a ordem de entrada de dados.
			CACHE[i][1][9] = 1'b1;				//Define os bits de LRU da via_1 com 1, implementando a ordem de entrada de dados.
		 end
	end
	
	always @(posedge clk) begin
		if(req_we) begin 	//Caso a ação seja de escrita.
			if(CACHE[data_index][0][13:10] != data_tag && CACHE[data_index][1][13:10] != data_tag) begin	//Caso a tag de parâmetro da via_0 seja diferente da tag encontrada no endereço selecionado e a tag de parâmetro da via_1 seja também diferente da mesma, ocorrerá um miss, e portanto a escrita na memória cache.
				if(CACHE[data_index][0][9] == 0) begin	//Caso o bit LRU, que define a ordem de entrada de dados, da via_0 seja igual a 0.
					LRU = 1'b1; //Define a entrada para a via_1;
				end
				else begin	//Caso o bit LRU, que define a ordem de entrada de dados, da via_0 seja igual a 1.
					LRU = 1'b0;	//Define que deverá ser escrito na via_0;
				end
				CACHE[data_index][LRU][7:0] = req_data;	//Escreve o dado no endereço de parâmetro previamente calculado pelo index, tag e offset.
				CACHE[data_index][LRU][8] = 1'b1;	//Define 1 para o bit de validade do dado selecionado pelo endereço de parâmetro.
				CACHE[data_index][LRU][9] = 1'b0;	//Define 0 para o bit escrito mais recetemente, ou seja, o dado selecionado pelo endereço de parâmetro, assim definindo a ordem da entrada LRU.
				CACHE[data_index][~LRU][9] = 1'b1;	//Define 1 para o bit escrito menos recetemente, ou seja, o dado não selecionado pelo endereço de parâmetro, assim definindo a ordem da entrada LRU.
				CACHE[data_index][LRU][13:10] = data_tag;	//Define uma nova tag para a linha de cache do dado selecionado pelo endereço de parâmetro.
				data_hit = 1'b0;		//Define para a saída que não ocorreu uma operação de hit.
				data_miss = 1'b1;		//Define para a saída que ocorreu uma operação de miss.
			end
			else begin	//Caso a tag de parâmetro seja igual da tag encontrada no endereço selecionado, ocorrerá um hit durante o processo de escrita na cache.
				data_hit = 1'b1;		//Define para a saída que ocorreu uma operação de hit.
				data_miss = 1'b0;		//Define para a saída que não ocorreu uma operação de miss.
			end
		end
		else begin
			if(CACHE[data_index][0][13:10] == data_tag) begin //Caso a tag de parâmetro da via_0 seja igual da tag encontrada no endereço selecionado, ocorrerá um hit durante a leitura da cache.
				if(CACHE[data_index][0][8] == 1)	begin //Caso o dado encontrado no endereço selecionado seja válido, ou seja, o bit de validade seja igual a 1.
					data_read = CACHE[data_index][0][7:0];	//Ocorre a leitura do dado no index e no número do bloco selecionados pelo endereço de parâmetro.
					data_hit = 1'b1;		//Define para a saída que ocorreu uma operação de hit.
					data_miss = 1'b0;		//Define para a saída que não ocorreu uma operação de miss.
				end
			end
			else if(CACHE[data_index][1][13:10] == data_tag) begin //Caso a tag de parâmetro da via_1 seja igual da tag encontrada no endereço selecionado, ocorrerá um hit durante a leitura da cache.
				if(CACHE[data_index][1][8] == 1)	begin //Caso o dado encontrado no endereço selecionado seja válido, ou seja, o bit de validade seja igual a 1.
					data_read = CACHE[data_index][1][7:0];	//Ocorre a leitura do dado no index e no número do bloco selecionos pelo endereço de parâmetro.
					data_hit = 1'b1;			//Define para a saída que ocorreu uma operação de hit.
					data_miss = 1'b0;		//Define para a saída que não ocorreu uma operação de miss.
				end
			end
			else begin	//Caso a tag de parâmetro seja diferente da tag encontrada no endereço selecionado, ocorrerá um miss durante a leitura da cache, portanto faz-se necessário a busca na memória RAM, que não foi implementada.
				data_hit = 1'b0;			//Define para a saída que não ocorreu uma operação de hit.
				data_miss = 1'b1;		//Define para a saída que ocorreu uma operação de miss.
			end
		end
	end
endmodule

//Modulo principal.
module cache_memory(
	input [17:0]SW,
	output [0:6]HEX0, output [0:6]HEX1, output [0:6]HEX2, output [0:6]HEX3, output [0:6]HEX4, output [0:6]HEX5, output [0:6]HEX6, output [0:6]HEX7,
	output [0:0]KEY,
	output [7:0]LEDG,
	output [17:0]LEDR
);
	wire [7:0] input_data;
	wire [7:0] input_addr;
	wire [7:0] output_cache;	
	wire [6:0] output_data_0, output_data_1, output_addr_0, output_addr_1, output_cache_data_0, output_cache_data_1;
	
	cache_data CD(SW[16], SW[15:8], SW[7:0], SW[17], output_cache, LEDR[0], LEDR[1]);
			
	assign HEX2 = 7'b1111111;		//Desligar o display HEX2.
	assign HEX3 = 7'b1111111;		//Desligar o display HEX3.
	assign LEDR[17] = SW[17]; 		//Sinaliza que o sinal de escrita está ativo.
	assign LEDR[16] = KEY[0];		//Sinaliza que o clock está ativo.
	assign input_addr = SW[15:8];	//Define em addr o endereço de parâmetro passado por meio dos switches.
	assign input_data = SW[7:0];
	
	//Sinaliza os dados de entrada da mémoria associativa por duas vias.
	display D0(input_data, output_data_0, output_data_1);		//Divisão dos bits do número em SW[7:0].
	convert_to_hexadecimal H0(output_data_0, HEX4);		//Conversor de hexadecimal.
	convert_to_hexadecimal H1(output_data_1, HEX5);		//Conversor de hexadecimal.
	//Sianliza o endereço de entrada da mémoria associativa por duas vias.
	display D1(input_addr, output_addr_0, output_addr_1); 	//Divisão dos bits do número em SW[15:11].
	convert_to_hexadecimal H2(output_addr_0, HEX6);		//Conversor de hexadecimal.
	convert_to_hexadecimal H3(output_addr_1, HEX7);		//Conversor de hexadecimal.
	//Sinaliza a saída de dados da mémoria associativa por duas vias.
	display D2(output_cache, output_cache_data_0, output_cache_data_1);	//Divisão dos bits do número de sáida da cache.
	convert_to_hexadecimal H4(output_cache_data_0, HEX0);		//Conversor de hexadecimal.
	convert_to_hexadecimal H5(output_cache_data_1, HEX1);		//Conversor de hexadecimal.
endmodule
