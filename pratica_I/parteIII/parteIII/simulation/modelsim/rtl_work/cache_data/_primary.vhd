library verilog;
use verilog.vl_types.all;
entity cache_data is
    port(
        clk             : in     vl_logic;
        req_addr        : in     vl_logic_vector(7 downto 0);
        req_data        : in     vl_logic_vector(7 downto 0);
        req_we          : in     vl_logic;
        data_read       : out    vl_logic_vector(7 downto 0);
        data_hit        : out    vl_logic;
        data_miss       : out    vl_logic
    );
end cache_data;
