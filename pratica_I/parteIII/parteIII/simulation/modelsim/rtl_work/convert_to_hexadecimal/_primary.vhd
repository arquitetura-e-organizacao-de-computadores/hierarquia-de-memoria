library verilog;
use verilog.vl_types.all;
entity convert_to_hexadecimal is
    port(
        n               : in     vl_logic_vector(6 downto 0);
        n_hex           : out    vl_logic_vector(6 downto 0)
    );
end convert_to_hexadecimal;
