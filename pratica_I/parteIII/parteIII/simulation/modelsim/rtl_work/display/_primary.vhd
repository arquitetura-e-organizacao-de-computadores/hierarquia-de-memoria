library verilog;
use verilog.vl_types.all;
entity display is
    port(
        n               : in     vl_logic_vector(7 downto 0);
        ML_HEXO         : out    vl_logic_vector(6 downto 0);
        ML_HEX1         : out    vl_logic_vector(6 downto 0)
    );
end display;
