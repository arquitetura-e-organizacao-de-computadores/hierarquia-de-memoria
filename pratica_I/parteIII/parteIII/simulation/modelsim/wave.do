onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /cache_memory/SW
add wave -noupdate /cache_memory/HEX7
add wave -noupdate /cache_memory/HEX6
add wave -noupdate /cache_memory/HEX5
add wave -noupdate /cache_memory/HEX4
add wave -noupdate /cache_memory/HEX1
add wave -noupdate /cache_memory/HEX0
add wave -noupdate /cache_memory/LEDR
add wave -noupdate /cache_memory/LEDG
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {724 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 178
configure wave -valuecolwidth 48
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {651 ps} {915 ps}
view wave 
wave clipboard store
wave create -driver freeze -pattern repeater -initialvalue 000000000000000000 -period 50ps -sequence { 000000000000000000 010000000000000000 000000000000000000 100000000000001111 110000000000001111 100000000000001111 000000000000000000  } -repeat forever -range 17 0 -starttime 0ps -endtime 1000ps sim:/cache_memory/SW 
WaveExpandAll -1
wave modify -driver freeze -pattern repeater -initialvalue 000000000000000000 -period 50ps -sequence { 000000000000000000 010000000000000000 110000000000001111 100000000000000000 010000000000000000 010000000000000000  } -repeat forever -range 17 0 -starttime 0ps -endtime 1000ps Edit:/cache_memory/SW 
wave modify -driver freeze -pattern repeater -initialvalue 000000000000000000 -period 50ps -sequence { 000000000000000000 010010000000000000 110010000000001111 100010000000000000 010010000000000000 010010000000000000  } -repeat forever -range 17 0 -starttime 0ps -endtime 1000ps Edit:/cache_memory/SW 
WaveCollapseAll -1
wave clipboard restore
